#!/usr/bin/env python
import sys
import json
from pprint import pprint

def usage():
	print('usage: %s FILE WRITE[y|n]' % sys.argv[0])
	sys.exit(1)

if len(sys.argv) < 3:
	usage()
	
ifile = sys.argv[1]
tofile = sys.argv[2].lower()

if tofile not in 'yn':
	usage()

if tofile == 'y':
	tofile = True
else:
	tofile = False 

print(ifile)
with open(ifile) as fh:
	obj = json.load(fh)
	if 'nodes' in obj:
		print('nodes : %d' % len(obj['nodes']))
		if tofile:
			nfile = 'nodes.json'
			with open(nfile, 'w') as nfh:
				json.dump({'nodes': obj['nodes']}, nfh)

	if 'links' in obj:
		print('links : %d' % len(obj['links']))
		if tofile:
			lfile = 'links.json'
			with open(lfile, 'w') as lfh:
				json.dump({'links': obj['links']}, lfh)
	
	
	



