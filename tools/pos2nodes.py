#!/usr/bin/env python
'''
    For the Genome Constellation web app.
    Add the pre-computed positions to 'nodes' object

    Run :
    pos2nodes.py -n References_nodes.json -p precomputedpos.json

    where precomputedpos.json contains the content of listNodePosition() from web dev tool

    Author: Shijie Yao
    Last modified : 5/17/2017

'''
import sys
import json
from argparse import ArgumentParser
from pprint import pprint




def dump2file(fname, alist):
    fname = "links_%d.json" % fcnt
    print(' out %s ...' % fname)
    with open(fname, 'w') as fh:
        json.dump({'links': alist}, fh)

def outfilename(ifile):
    toks = ifile.rsplit('.', 1)
    if len(toks)==2:
        ofile = '%s.updatedpos.%s' % (toks[0], toks[1])
    else:
        ofile = '%s.updatedpos' % toks[0]

    return ofile

def do(nfile, pfile, blocksize=None, exlist=None):
    with open(nfile) as fh:
        nobj = json.load(fh)

    with open(pfile) as fh:
        pobj = json.load(fh)

    for node in nobj['nodes']:
        nid = node['id']
        if nid in pobj:
            x = pobj[nid][0]
            y = pobj[nid][1]
            node['x'] = x
            node['y'] = y
            #print('%s %f %f' % (nid, x, y))
        else:
            if exlist:
                if nid in exlist:
                    print ('%s : no pos node in exlist' % nid)
                else:
                    print ('%s : no pos node NOT in exlist' % nid)
            # else:
            #     print ('%s : no pos node' % nid)
            if 'x' in node:
                del(node['x'])
            if 'y' in node:
                del(node['y'])
    #

    ofile = outfilename(nfile)
    with open(ofile, 'w') as fh:
        json.dump(nobj, fh)

    print('Please rename %s to the nodes file for your web app.' % ofile)

if __name__ == "__main__":
    print('\n')
    NAME = __file__ #sys.argv[0]
    VERSION = "1.0.0"


    # Parse options
    USAGE = "\n* Add precomputed node positions to json data objects %s\n" % (VERSION)

    # JOB_CMD += "/global/homes/b/brycef/git/jgi-rqc/framework/test/pipeline_test.py --fastq %s --output-path %s"
    JOB_CMD = ' '.join(sys.argv)
    PARSER = ArgumentParser(usage=USAGE)

    PARSER.add_argument("-n", "--node", dest="nfile", type=str, help="input json file with nodes", required=True)
    PARSER.add_argument("-p", "--position", dest="pfile", type=str, help="input json file with positions", required=True)
    PARSER.add_argument("-ex", "--excluded", dest="efile", type=str, help="input json file excluded nodes")
    PARSER.add_argument("-b", "--block-size", dest="blksize", type=int, help="block size; for spliting output files")
    PARSER.add_argument("-v", "--version", action="version", version=VERSION)

    ARGS = PARSER.parse_args()

    print('Node file : %s' % ARGS.nfile)
    print('position file : %s' % ARGS.pfile)
    ofile = None
    if ARGS.blksize:
        print('multiple out put files of %d record' % ARGS.blksize)
    else:
        print('out put to %s' % outfilename(ARGS.nfile))

    excludes = None
    if ARGS.efile:
        with open(ARGS.efile) as fh:
            eobj = json.load(fh)
            if type(eobj) == dict:
                excludes = eobj.keys()
            elif type(eobj) == list:
                excludes = eboj

    do(nfile=ARGS.nfile, pfile=ARGS.pfile, blocksize=ARGS.blksize, exlist=excludes)

    print('\n')