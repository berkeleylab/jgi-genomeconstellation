#!/usr/bin/env python
import sys
import os
import json
import glob
from pprint import pprint

def usage():
    print('usage: %s LINK-JSON/DIR_OF_SPLITED_JSON BLOCK-SIZE' % sys.argv[0])
    sys.exit(1)


def dump2file(fname, alist):
    print(' out %s with %d values ...' % (fname, len(alist)))
    with open(fname, 'w') as fh:
        json.dump({'links': alist}, fh)

def concat_files(adir):
    fname = os.path.join(adir, 'LINK.json')
    obj = {}
    
    pat = os.path.join(adir, 'links_*.json')
    lfiles = glob.glob(pat)

    for lf in lfiles:
        with open (lf) as fh:
            tmp = json.load(fh)
            if 'links' not in tmp:
                print('Error : bad links file : ' % lf)
                exit(1)
            if len(obj) == 0:
                obj = tmp
            else:
                obj['links'] = obj['links'] + tmp['links']

    print('write tmp concat link file : %s; %d links' % (fname, len(obj['links'])))
    with open(fname, 'w') as fh:
        json.dump(obj, fh)
    
    return obj
    
if len(sys.argv) < 3:
    usage()

inflag = sys.argv[1]
blksize = int(sys.argv[2])
odir = os.getcwd()
obj = None

if os.path.isdir(inflag):
    print("split link file dir: %s; out block size: %d\n" % (inflag, blksize))
    obj = concat_files(inflag)
    odir = inflag
else:
    print("in file: %s; out block size: %d\n" % (inflag, blksize))
    with open(inflag) as fh:
        obj = json.load(fh)
        if 'links' in obj:
            print('input file contains %d links.' % len(obj['links']))
        else:
            print('Error : bad links file : ' % inflag)
            exit(1)

if obj:
    links = None
    if 'links' in obj:
        links = obj['links']
    elif type(obj) == list:
        links = obj

    if links:
        print('links : %d' % len(links))

        tmplist = []
        fcnt = 0
        for lin in links:
            if len(tmplist) == blksize:
                fname = os.path.join(odir, "links_%d.json" % fcnt)
                dump2file(fname, tmplist)
                fcnt += 1
                tmplist = [lin]
            else:
                tmplist.append(lin)

        if len(tmplist) > 0:
            fname = os.path.join(odir, "links_%d.json" % fcnt)
            dump2file(fname, tmplist)
    else:
        print('%s is not a link file' % inflag)
        sys.exit(1)
