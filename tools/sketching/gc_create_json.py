#!/usr/bin/env python
import csv
import json
import sys

links_file_name = sys.argv[2]
links_csvfile = open(links_file_name, 'r')
links_json_obj = []

nodes_file_name = sys.argv[1]
nodes_csvfile = open(nodes_file_name, 'r')

annotation_json_obj = []
links_json_obj = []

fieldnames = ("id","reference","taxonomy")
reader = csv.DictReader(nodes_csvfile, fieldnames,delimiter='\t', quoting=csv.QUOTE_NONE)
for row in reader:
	row["reference"]=int(row["reference"])
	annotation_json_obj.append(row)
    
fieldnames = ("source","target","value")
reader = csv.DictReader(links_csvfile, fieldnames, delimiter='\t', quoting=csv.QUOTE_NONE)
for row in reader:
	row["value"]=float(row["value"])
	links_json_obj.append(row)

jsonfile = open(nodes_file_name + '.json', 'w')
jsonfile.write(json.dumps(annotation_json_obj))
jsonfile.close()
jsonfile = open(link_file_name + '.json', 'w')
jsonfile.write(json.dumps({'links' : links_json_obj })
jsonfile.close()

nodes_csvfile.close()
links_csvfile.close()
