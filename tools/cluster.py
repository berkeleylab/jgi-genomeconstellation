#!/usr/bin/env python

# by Zhong Wang @ lbl . gov

import sys
import pandas as pd
import numpy as np
import json
from sklearn.cluster import DBSCAN
from scipy.sparse import coo_matrix

def cluster(nodes_json, links_json, cluster_output, eps=60):
    """
    clustering genomes
    nodes_json is the node json file
    links_json is a list of links_json files
    
    """
    nodes = json.load(open(nodes_json,'r'))
    links = []
    for i in range(len(links_json)):
        l = json.load(links_json[i], 'r')
        links.extend(l["links"])

    links = pd.DataFrame(links)  
    links.rename(columns={'source':'src', 'target':'des', 'value':'wt'}, inplace=True)

    # taxonomy annotation
    taxa = {}
    for n in nodes:
        (ID, row) = (n['id'], n['taxonomy'])
        t = [
            row['superkingdom'], 
            row['phylum'], 
            row['class'], 
            row['order'], 
            row['family'],
            row['genus'],
            row['species']
        ]
        taxa[ID] = t


    # clustering
    novel = links.copy()
    # convert similarity to distance
    novel['wt'] = [100.0-i for i in novel['wt']]
    # uniq set of nodes in the data
    nodes = set(np.concatenate((novel['src'].values, novel['des'].values), axis=0))
    # index nodes using integers, construct a bi-directional dict for look up 
    nodes_dict = {}
    for v,k in zip(range(len(nodes)), nodes):
        nodes_dict[k] = v
        nodes_dict[v] = k
    # rows and cols for the sparse matrix
    row = np.array([nodes_dict[i] for i in novel['src'].values])
    col = np.array([nodes_dict[i] for i in novel['des'].values])
    # construct a coo matrix using triplets, then convert it to csr matrix
    X = coo_matrix((novel['wt'].values, (row, col)), shape=(len(nodes), len(nodes))).tocsr()
    clustering = DBSCAN(eps=eps, min_samples=2, metric='precomputed', n_jobs=-1).fit(X)
    index = np.array(range(len(clustering.labels_)))
    c = Counter(clustering.labels_)

    clustered_genomes = []
    for i in c.most_common():
        genomes = [nodes_dict[j] for j in index[clustering.labels_ == i[0]]]
        clustered_genomes.append([i[0], ','.join(genomes)])
    clustered_genomes = pd.DataFrame(clustered_genomes, columns=['Cluster_label', 'Genomes'])
    clustered_genomes.to_csv(cluster_output, index=False, sep='\t')
    
def main():

    cluster_output = sys.argv[1]
    eps = float(sys.argv[2])
    links_json = sys.argv[3:]
    if (eps<0) or len(links_json)<1:
        print("Usage:\n" + sys.argv[0] + " cluster_output_file eps_cutoff links_json_0 links_json_1 ...\n")  
    else:
        cluster(nodes_json, links_json, cluster_output, eps=eps)
if __name__ == "__main__":
    main()    