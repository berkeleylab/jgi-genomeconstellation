#ifndef JGI_GC_H_
#define JGI_GC_H_

#include <cstdint>
#include <iostream>
#include <ostream>
#include <istream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdarg>
#include <iomanip>
#include <numeric>
#include <cassert>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef __APPLE__
#include <sys/sysctl.h>
#include <mach/mach.h>
#else
#include <sys/sysinfo.h>
#endif
#include <zlib.h>

// popcnt internal call
#include <immintrin.h>
#include <x86intrin.h>

// force BOOST ublas optimizations
#define BOOST_UBLAS_INLINE inline
#define BOOST_UBLAS_CHECK_ENABLE 0
#define BOOST_UBLAS_USE_FAST_SAME
#define BOOST_UBLAS_TYPE_CHECK 0

#include <unordered_map>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include "murmur3.h"
#include "OpenMP.h"
#include "ProgressTracker.h"
#include "kseq.h"
KSEQ_INIT(gzFile, gzread)

static const std::string version = "0.21.1";

using std::endl;
using std::cout;
using std::cerr;
namespace po = boost::program_options;

static int numThreads = 0;
//static unsigned long long seed = 0;

static timeval t1, t2;
static bool verbose = false;
static bool debug = false;

static void verbose_message(const char* format, ...);

static const char line_delim = '\n';
static const char tab_delim = '\t';
static const char comma_delim = ',';

//static double ANI_CUTOFF = 1;//5.22;
static int FP_PREFIX = INT_MAX;

#define CALLOC_LEN(p,l) ((p) = (typeof(p))calloc(1,(l)))
#define MALLOC_LEN(p,l) ((p) = (typeof(p))malloc(l))

typedef double (*FnPtr)(double, double, double, double);

double ForbesIIs(double a, double b, double c, double d) {
	double n = a + b + c + d;
	double p = (a+b) / n * (a+c) / n;
	return (a - n * p) / sqrt(p * (1 - p) * n);
}

FnPtr metricf = ForbesIIs;

inline char complement(char base) {
	switch (base) {
	case 'A':
		return 'T';
	case 'C':
		return 'G';
	case 'G':
		return 'C';
	case 'T':
		return 'A';
	default:
		return 'N';
	}
}

inline bool reverseComplement(const char *in, char *_out, int len) {
	bool success = true;
	char *out = _out + len;
	*out = '\0';
	for (int i = 0; i < len; i++) {
		out--;
		*out = complement(in[i]);
		success &= ('N' != *out);
	}
	assert(out == _out);
	return success;
}

class Fingerprint {

public:
	typedef uint64_t Word;
	typedef uint32_t MinFractionWord;
	typedef Fingerprint * FingerprintPtr;
	typedef const Fingerprint * ConstFingerprintPtr;

public:
	//
	// static variables tracking fingerprint size
	//

	static const int unrollWords = 16;
	static const int wordSize = sizeof(Word);
	static const int wordBits = sizeof(Word) * 8;
	static Word &getNumBits() {
		static Word _ = 0;
		return _;
	}
	static MinFractionWord &getMinFraction() {
		static MinFractionWord _ = 1;
		return _;
	}

	// This method must be called before any Fingerprints are constructed!
	static void setNumBits(Word numBits, MinFractionWord minFraction = 1) {
		assert(numBits > 0);
		if (getNumBits() != 0 && getNumBits() != numBits) {
			std::cerr << "You can only set the fingerprint bits once!" << std::endl;
			exit(1);
		}
		getNumBits() = ((numBits + unrollWords * wordBits - 1) / (unrollWords * wordBits)) * unrollWords * wordBits;
		getMinFraction() = minFraction;
		if(debug)
			verbose_message("Set fingerprint to %ld bits, minFraction of %d\n", getNumBits(), minFraction);
		assert(getNumBits() >= numBits);
		getNumBytes() = (getNumBits() + 7) / 8;
		getNumWords() = (getNumBytes() + sizeof(Word) - 1) / sizeof(Word);
		if (getNumWords() % unrollWords != 0) {
			std::cerr << "Please select a bit count that is an even multiple of: " << 64 * unrollWords << std::endl;
			exit(1);
		}
	}

	static Word &getNumBytes() {
		static Word _ = 0;
		return _;
	}

	static Word &getNumWords() {
		static Word _ = 0;
		return _;
	}

	//
	// static methods to count and manipulate bits
	//

	static void set_bit(Word *bits, Word idx) {
		assert(getNumBits() > 0);
		Word word;
		uint8_t bit;
		Word bitmask = 1L;

		assert(idx < getNumBits());
		word = idx >> 6; // div 64
		assert(word >= 0 && word < getNumWords());
		bit = (uint8_t) (idx & 0x3f); // mod 64
		assert(bit >= 0 && bit < 64);
		bitmask <<= bit;
		bits[word] |= bitmask;

		return;
	}

	inline static uint8_t get_bit(Word *bits, Word idx) {
		assert(getNumBits() > 0);
		Word word;
		uint8_t bit;
		Word bitmask = 1L;

		assert(idx < getNumBits());
		word = idx >> 6; // div 64
		assert(word >= 0 && word < getNumWords());
		bit = (uint8_t) (idx & 0x3f); // mod 64
		assert(bit >= 0 && bit < 64);
		bitmask <<= bit;
		return (bits[word] & bitmask) != 0;
	}

	//refer to http://danluu.com/assembly-intrinsics/
	static inline Word builtin_popcnt_unrolled_errata_manual(const Word* buf, Word len) {
		assert(len % 4 == 0);
		Word cnt[4];
		for (int i = 0; i < 4; ++i) {
			cnt[i] = 0;
		}

		for (Word i = 0; i < len; i += 4) {
			__asm__(
					"popcnt %4, %4  \n\t"
					"add %4, %0     \n\t"
					"popcnt %5, %5  \n\t"
					"add %5, %1     \n\t"
					"popcnt %6, %6  \n\t"
					"add %6, %2     \n\t"
					"popcnt %7, %7  \n\t"
					"add %7, %3     \n\t"
					: "+r" (cnt[0]), "+r" (cnt[1]), "+r" (cnt[2]), "+r" (cnt[3])
					: "r" (buf[i]), "r" (buf[i+1]), "r" (buf[i+2]), "r" (buf[i+3])
			);
		}
		return cnt[0] + cnt[1] + cnt[2] + cnt[3];
	}

	// simple loop
	static inline Word builtin_popcnt(const Word *buf, Word len) {
		assert(getNumBits() > 0);
		assert(len <= getNumWords());
		Word cnt = 0;
		for (Word i = 0; i < len; i++) {
			cnt += _popcnt64(buf[i]);
		}
		return cnt;
	}

	//This is better when most bits in buf are 0
	//It uses 3 arithmetic operations and one comparison/branch per "1" bit in x.
	static inline Word sparse_popcnt(const Word *buf, Word len) {
		assert(getNumBits() > 0);
		assert(len <= getNumWords());
		Word total = 0;
		for (Word i = 0; i < len; i++) {
			Word x = buf[i];
			for (; x; total++)
				x &= x - 1;
		}
		return total;
	}

	static inline Word popcnt(const Word *buf, Word len) {
		return builtin_popcnt_unrolled_errata_manual(buf, len);
		//return sparse_popcnt(buf,len);
		//return builtin_popcnt(buf,len);
	}

	static void bitAndPopcnt(ConstFingerprintPtr *x, ConstFingerprintPtr *y, int lenx, int leny, Word *bitAndCounts) {
		assert(getNumBits() > 0);
		assert(getNumBytes() / getNumWords() == sizeof( Word ));
		assert(getNumBytes() % getNumWords() == 0);
		assert(getNumWords() % unrollWords == 0);
		Word counts[lenx * leny];
		Word buf[unrollWords];
		Word const *offsetx[lenx], *offsety[leny], *fpx, *fpy;
		int idx;
		for (idx = 0; idx < lenx * leny; idx++)
			counts[idx] = 0;
		for (int k = 0; k < lenx; k++)
			offsetx[k] = x[k]->get();
		for (int k = 0; k < leny; k++)
			offsety[k] = y[k]->get();

		for (Word b = 0; b < getNumWords(); b += unrollWords) {
			idx = 0;
			for (int i = 0; i < lenx; i++) {
				fpx = offsetx[i] + b;
				for (int j = 0; j < leny; j++) {
					fpy = offsety[j] + b;
					for (int k = 0; k < unrollWords; k++) {
						buf[k] = fpx[k] & fpy[k];
					}
					counts[idx++] += popcnt(buf, unrollWords);
				}
			}
		}
		for (int k = 0; k < lenx * leny; k++)
			bitAndCounts[k] = counts[k];
		return;
	}

public:
	//
	// class methods
	//

	Fingerprint() :
			_bitson(0), _index(-1), _seq(NULL), _mask(NULL) {
		assert(getNumBits() > 0);
	}
	Fingerprint(const std::string fastaFile, int k, int index = -1) :
			_bitson(0), _seq(NULL), _mask(NULL) {
		assert(getNumBits() > 0);
		genome_to_bitstring(fastaFile, k, index);
	}
	virtual ~Fingerprint() {
		reset(true);
	}
	// copy constructor
	Fingerprint(const Fingerprint &that) {
		_bitson = that._bitson;
		_index = that._index;
		memcpy(get(), that.get(), getNumBytes());
		_mask = that._mask;
	}
	// move constructor
	Fingerprint(Fingerprint &&that) {
		_bitson = that._bitson;
		_index = that._index;
		_seq = that._seq;
		_mask = that._mask;
		that.reset(false);
	}
	// assignment operator
	Fingerprint &operator=(Fingerprint that) {
		std::swap(_bitson, that._bitson);
		std::swap(_seq, that._seq);
		std::swap(_mask, that._mask);
		return *this;
	}

	void reset(bool deleteMem = true) {
		_bitson = 0;
		_index = -1;
		isCached = false;
		if (_seq && deleteMem)
			delete[] _seq;
		_seq = NULL;
	}
	// move constructor

	// return the raw fingerprint data
	const Word *get() const {
		assert(getNumBits() > 0);
		assert(_seq);
		return _seq;
	}
	Word *get() {
		assert(getNumBits() > 0);
		if (!_seq)
			init();
		assert(_seq);
		return _seq;
	}

	// return the cached bitson count
	inline Word getBitsOn() {
		assert(getNumBits() > 0);
		return isCached ? _bitson : popcount();
	}

	// allocate memory, set all bits false
	void init() {
		assert(getNumBits() > 0);
		if (!_seq) {
			_seq = new Word[getNumWords()];
		}
		if (!_seq) {
			cerr << "Could not allocate another fingerprint!" << endl;
			exit(1);
		}
		clear();
		//cout << "init: " << _seq << endl;
	}

	// set all bits false
	void clear() {
		assert(getNumBits() > 0);
		Word *seq = get();
		for (Word i = 0; i < getNumWords(); i++) {
			seq[i] = 0;
		}
		assert(popcount() == 0);
		_bitson = 0;
		_index = -1;
		isCached = false;
	}

	void applyMask(Fingerprint *mask) {
		if (mask == NULL)
			return;
		Word *seq = get();
		const Word *mask_seq = mask->get();
		for (Word k = 0; k < getNumWords(); k += unrollWords) {
			for (int l = 0; l < unrollWords; l++) {
				seq[k + l] &= mask_seq[k + l];
			}
		}
		popcount();
	}

	void add(Fingerprint *other) {
		if (other == NULL)
			return;
		Word *seq = get();
		const Word *other_seq = other->get();
		for (Word k = 0; k < getNumWords(); k += unrollWords) {
			for (int l = 0; l < unrollWords; l++) {
				seq[k + l] |= other_seq[k + l];
			}
		}
		popcount();
	}

	void intersect(Fingerprint *other) {
		if (other == NULL)
			return;
		Word *seq = get();
		const Word *other_seq = other->get();
		for (Word k = 0; k < getNumWords(); k += unrollWords) {
			for (int l = 0; l < unrollWords; l++) {
				seq[k + l] &= other_seq[k + l];
			}
		}
		popcount();
	}

	// count and cache the number of true bits
	inline Word popcount() {
		assert(getNumBits() > 0);
		_bitson = popcnt(get(), getNumWords());
		assert(_bitson <= getNumBits());
		assert(_bitson == popcnt(get(), getNumWords()));
		isCached = true;
		return _bitson;
	}

	// set a single bit
	inline void setBit(Word idx) {
		assert(getNumBits() > 0);
		assert(idx >= 0 && idx <= getNumBits());
		set_bit(get(), idx);
	}

	Word bitAndPopcnt(const Fingerprint &other) const {
		assert(getNumBits() > 0);
		Word numWords = getNumWords();
		Word andCount = 0;
		assert(numWords % unrollWords == 0);
		Word x[unrollWords];
		const Word *seq = get(), *other_seq = other.get();
		for (Word k = 0; k < numWords; k += unrollWords) {
			for (int l = 0; l < unrollWords; l++) {
				x[l] = seq[k + l] & other_seq[k + l];
			}

			andCount += popcnt(x, unrollWords);
		}
		assert(_bitson >= (Word ) andCount);
		assert(other._bitson >= (Word ) andCount);

		return andCount;
	}

	inline double calcScore(Word bitAndCount, const Fingerprint &other, FnPtr f) const {
		assert(isCached);
		double a = bitAndCount;
		double b = _bitson - a;
		double c = other._bitson - a;
		double d = ((_mask == NULL) ? getNumBits() : _mask->getBitsOn()) - (a + b + c);
		assert(a >= 0.0);
		assert(b >= 0.0);
		assert(c >= 0.0);
		assert(d >= 0.0);

		//cout << std::setprecision(15) << a << "," << b << "," << c << "," << d << endl;

		double score = f(a, b, c, d);
		return score;
	}

	double calcScore(const Fingerprint &other, FnPtr f) const {
		assert(getNumBits() > 0);
		Word andCount = bitAndPopcnt(other);
		return calcScore(andCount, other, f);
	}

	void calcScores(ConstFingerprintPtr *fps, int fplen, FnPtr f, float *scores) const {
		Word bitAndCounts[fplen];
		ConstFingerprintPtr x[1];
		x[0] = this;
		Fingerprint::bitAndPopcnt(x, fps, 1, fplen, bitAndCounts);
		for (int i = 0; i < fplen; i++) {
			scores[i] = calcScore(bitAndCounts[i], *fps[i], f);
		}
	}

	Word setBitsForSequence(const char *seq, Word len, int k) {
		assert(getNumBits() > 0);
		Word hash[2];
		Word numBits = Fingerprint::getNumBits();
		MinFractionWord minFraction = Fingerprint::getMinFraction();
		MinFractionWord minFractionMask = minFraction - 1;
		const char *kmer;
		char revcomp[k + 1];
		revcomp[k] = '\0';

		if (len < (Word) k)
			return 0;

		//experiment of incomplete genomes
//		double comp = .5; //90% completeness
//		Word s = rand() % (Word)(len * (1. - comp));
//		Word len2 = min(s + (Word)(len * comp), len);
//
//		if (len2 < (Word) k)
//			return 0;

		//std::cout << s << " : " << e << " / " << len << std::endl;

		for (Word i = 0; i < len - k + 1; i++) { //
			kmer = seq + i;
			bool ok = reverseComplement(kmer, revcomp, k);
			if (!ok)
				continue;

			if (strcmp(kmer, revcomp) <= 0) {
				// set bit for fwd
				MurmurHash3_x64_128(kmer, k, 0L, &hash);
			} else {
				// set bit for revcomp
				MurmurHash3_x64_128(revcomp, k, 0L, &hash);
			}
			if (minFraction > 1) {
				// check a few of the bits for the minFraction value
				MinFractionWord *h = (MinFractionWord*) hash;
				if ( (*h & minFractionMask) != minFractionMask) continue;
			}
			setBit(llabs((hash[0] + hash[1]) % numBits));
		}
		return len - k + 1;
	}

	// dump this fingerprint to an output
	std::ostream &serialize(std::ostream &os) {
		assert(getNumBits() > 0);
		os.write(reinterpret_cast<char*>(&_index), sizeof(_index));
		os.write((const char *) get(), getNumBytes());
		//cout << "serialized: " << _index << ", popcount(): " << popcount() << endl;
		return os;
	}
	void deserialize(std::istream &is) {
		assert(getNumBits() > 0);
		popcount();
		is.read(reinterpret_cast<char*>(&_index), sizeof(_index));
		is.read((char *) get(), getNumBytes());
		popcount();
		//cout << "deserialized: " << _index << ", popcount(): " << popcount() << endl;
	}

	// generate a fingerprint from a fasta file
	void genome_to_bitstring(const std::string fastaFile, int k, int index = -1) {
		assert(getNumBits() > 0);
		//check file type

		gzFile f = gzopen(fastaFile.c_str(), "r");
		if (f == NULL) {
			std::cerr << "[Error!] can't open the sequence fasta file: " << fastaFile << std::endl;
			exit(1);
		}
		kseq_t *kseq = kseq_init(f);

		int64_t len, totalLen = 0, numSeqs = 0;

		clear();
		assert(_seq);
		while ((len = kseq_read(kseq)) > 0) {
			numSeqs++;
			std::transform(kseq->seq.s, kseq->seq.s + len, kseq->seq.s, ::toupper);
			totalLen += len;
			setBitsForSequence(kseq->seq.s, len, k);
		}

		kseq_destroy(kseq);
		kseq = NULL;
		gzclose(f);

		popcount();
		if (debug)
			verbose_message("Read %s with %d sequences, %lld bases, %lld bitson (%2.2f%% filled)\n", fastaFile.c_str(), numSeqs, totalLen, getBitsOn(), (double) getBitsOn() / getNumBits() * 100);
		assert(totalLen == 0 || getBitsOn() > 0);
		assert(getBitsOn() <= getNumBits());

		setIndex(index);
	}

	int getIndex() {
		return _index;
	}

	void setIndex(int index) {
		_index = index;
	}

private:
	Word _bitson;
	int _index;
	Word *_seq;
	Fingerprint *_mask;
	bool isCached = false;
};
typedef std::vector<Fingerprint> FingerprintVector;

static void verbose_message(const char* format, ...) {
	if (verbose) {
		gettimeofday(&t2, NULL);
		int elapsed = (int) (((t2.tv_sec - t1.tv_sec) * 1000.0 + (t2.tv_usec - t1.tv_usec) / 1000.0) / 1000.0); //seconds
		printf("[%02d:%02d:%02d] ", elapsed / 3600, (elapsed % 3600) / 60, elapsed % 60);
		va_list argptr;
		va_start(argptr, format);
		vfprintf(stdout, format, argptr);
		cout.flush();
		va_end(argptr);
	}
}

typedef std::pair<size_t, double> ScorePair;
bool cmp_score_inc(const ScorePair& i, const ScorePair& j) {
	return j.second > i.second; //decreasing
}
bool cmp_score_desc(const ScorePair& i, const ScorePair& j) {
	return j.second < i.second; //decreasing
}

//http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
inline bool exists (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

unsigned long long rdtsc() {
	unsigned int lo, hi;
	__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
	return ((unsigned long long) hi << 32) | lo;
}

#ifdef __APPLE__
vm_statistics_data_t vmStats;
mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
#else
struct sysinfo memInfo;
#endif
double totalPhysMem = 0.;

int parseLine(char* line) {
	int i = strlen(line);
	while (*line < '0' || *line > '9')
		line++;
	line[i - 3] = '\0';
	i = atoi(line);
	return i;
}

double getTotalPhysMem() {
	if (totalPhysMem < 1) {
#ifdef __APPLE__
	    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
	    if(kernReturn != KERN_SUCCESS)
	        return 0;
	    return (vm_page_size * (vmStats.wire_count + vmStats.active_count + vmStats.inactive_count + vmStats.free_count)) / 1024;
#else
		sysinfo(&memInfo);
		long long _totalPhysMem = memInfo.totalram;
		_totalPhysMem *= memInfo.mem_unit;
		totalPhysMem = (double) _totalPhysMem / 1024; //kb
#endif
	}
	return totalPhysMem;
}

//http://blog.csdn.net/hengshan/article/details/9201929
int getFreeMem() {
#ifdef __APPLE__
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS)
        return 0;
    return (vm_page_size * vmStats.free_count) / 1024;
#else
    FILE* file = fopen("/proc/meminfo", "r");
	size_t result = 0;
	char line[128];

	while (fgets(line, 128, file) != NULL) {
		if (strncmp(line, "MemFree:", 6) == 0 ||
			strncmp(line, "Buffers:", 6) == 0 ||
			strncmp(line, "Cached:", 6) == 0 ) { //|| strncmp(line, "SwapFree:", 6) == 0) {
			result += parseLine(line);
		}
	}
	fclose(file);
	return result; //Kb
#endif
}

double getUsedPhysMem() {
	return (getTotalPhysMem() - getFreeMem()) / 1024. / 1024.;
}

//s: unlogged raw gc score
inline double calc_ani(double s) {
	//valid range of s is (1,1e4) //(5.22, 10000)
	if (s < 1)
		s = 1;
	if (s > 10000)
		return 100;
	s = log10(s);
	double s2 = s * s;
	double s3 = s2 * s;
	double s4 = s3 * s;
	return std::min(59.7217645 + 10.6285891 * s - 4.1547144 * s2 + 2.0074825 * s3 - 0.2495517 * s4, 100.);
	//return std::min(-132.7747 + 261.9284 * s - 125.9953 * s2 + 28.0169 * s3 - 2.3126 * s4, 100.);
}

inline void findColRow (size_t idx, size_t ncol, size_t& col, size_t& row) {
	idx++;
	double tmp = sqrt(pow(2*ncol-1,2) - 4*2*idx);
	double k1 = ((2*ncol - 1) + tmp) / 2.;
	double k2 = ((2*ncol - 1) - tmp) / 2.;
	double k = k1 < ncol ? k1 : k2;

	col = ceil(k);
	size_t k_dn = ceil(k-1);
	row = idx - k_dn*ncol + k_dn*(k_dn+1)/2 + col;
	col--;
	row--;
}

const size_t countLines(const char* f) {
	size_t lines = 0;

	FILE * pFile;
	pFile = fopen(f, "r");

	if (pFile == NULL) {
		cerr << "[Error!] can't open input file " << f << endl;
		return 0;
	}

	while (EOF != fscanf(pFile, "%*[^\n]") && EOF != fscanf(pFile, "%*c"))
		++lines;

	fclose(pFile);

	return lines;
}

//todo add additional info to header like file version and total fp number etc
size_t getNumFPs(std::string& fname) {
	std::ifstream isFP(fname.c_str(), std::ios::binary);
	if (!isFP.is_open()) {
		cerr << "[Error!] can't open file " << fname << endl;
		exit(1);
	}

	int prefix = 0;
	isFP.read(reinterpret_cast<char*>(&prefix), sizeof(prefix));

	if (prefix != FP_PREFIX)
		return 0;

	isFP.seekg(0, isFP.end);
	uint64_t filesize = isFP.tellg();
	isFP.seekg(0, isFP.beg);

	uint64_t fileNumBits = Fingerprint::getNumBits() + sizeof(int) * 8; //bits
	assert(fileNumBits % 8 == 0);
	uint64_t fileNumBytes = fileNumBits / 8;

	return (filesize - sizeof(FP_PREFIX)) / fileNumBytes;
}

//template <typename ListType>
size_t fillFastaList(std::string& faList, std::list<std::pair<size_t, std::string> >& fastaList) {
	std::ifstream is(faList.c_str());
	if (!is.is_open()) {
		cerr << "[Error!] can't open the fasta list file " << faList << endl;
		exit(1);
	}
	size_t num_seqs = 0;
	while (is.good()) {
		std::string fa;
		getline(is, fa, line_delim);
		if (!fa.empty())
			fastaList.push_back(std::make_pair(num_seqs++, fa));
	}
	is.close();

	return fastaList.size();
}

size_t fillFastaList(std::string& faList, std::vector<std::pair<size_t, std::string> >& fastaList) {
	std::ifstream is(faList.c_str());
	if (!is.is_open()) {
		cerr << "[Error!] can't open the fasta list file " << faList << endl;
		exit(1);
	}
	size_t num_seqs = 0;
	while (is.good()) {
		std::string fa;
		getline(is, fa, line_delim);
		if (!fa.empty())
			fastaList.push_back(std::make_pair(num_seqs++, fa));
	}
	is.close();

	return fastaList.size();
}

#endif
