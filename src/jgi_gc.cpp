#include "jgi_gc.h"

static std::string query;
static std::string target;
static std::string outGC;
static std::string outFP;
static size_t batchIndex;
static size_t batchSize;
static const int K = 23;
static bool noANI = false;
static bool append = false;
static bool self = false;
static bool isQueryFP = false;
static bool isTargetFP = false;
static double cache = 50;
static double minANI = 70;
static double minScore = 18.7219; //10^1.27235

int main(int ac, char* av[]) {
	po::options_description desc("Allowed options", 110, 110 / 2);
        uint64_t minFraction = 1024;
	uint64_t numBits = 131072;
	desc.add_options()
			("help,h", "produce help message")
			("query,i", po::value<std::string>(&query), "Query file. Either a single column list file or precalculated fingerprints file. [Mandatory]")
			("target", po::value<std::string>(&target), "Target file. Either a single column list file or precalculated fingerprints file.")
			("outGC", po::value<std::string>(&outGC), "GC score output file.")
			("outFP", po::value<std::string>(&outFP), "Fingerprints output file.")
			("minANI", po::value<double>(&minANI)->default_value(70), "Minimum ANI threshold to output [60-100]")
			("minScore", po::value<double>(&minScore)->default_value(18.7219), "Minimum score")
			("noANI", po::value<bool>(&noANI)->zero_tokens(), "No ANI transformation. Keep raw GC Score.")
			("append", po::value<bool>(&append)->zero_tokens(), "Append new fingerprints to supplied reference fingerprint file.")
			("self", po::value<bool>(&self)->zero_tokens(), "Indicate query and target are the same. Set to true if target is missing.")
			("cache", po::value<double>(&cache)->default_value(50), "Percentage of system memory allocated for caching [1-90]")
			("numThreads,t", po::value<int>(&numThreads)->default_value(numThreads), "Number of threads to use (0: use all cores)")
			("batchIndex", po::value<size_t>(&batchIndex)->default_value(0), "Batch index (0: the first)")
			("batchSize", po::value<size_t>(&batchSize)->default_value(0), "Batch size (0: no batches)")
                        ("minFraction" , po::value<uint64_t>(&minFraction)->default_value(minFraction), "(expert) Minimum hash threshold. 1 kmer out of minFraction bases in the genome will be added to the fingerprint. It must be power of 2 (1==all)")
                        ("numBits" , po::value<uint64_t>(&numBits)->default_value(numBits), "(expert) Number of bits in the fingerprint. It must be a power of 2 and >=2048")
			("debug,d", po::value<bool>(&debug)->zero_tokens(), "Debug output")
			("verbose,v", po::value<bool>(&verbose)->zero_tokens(), "Verbose output");

	po::variables_map vm;
	po::store(po::command_line_parser(ac, av).options(desc).allow_unregistered().run(), vm);
	po::notify(vm);

	if (vm.count("help") || query.empty()) {
		cerr << "\nJGI-GC: Genome Constellation (version " << version << "; " << __DATE__ << " " << __TIME__ << ")" << endl;
		cerr << "by Don Kang (ddkang@lbl.gov), Rob Egan, Derek Macklin, Jeff Froula, and Zhong Wang\n" << endl;
		cerr << desc << endl << endl;

		if (!vm.count("help")) {
			if (query.empty()) {
				cerr << "[Error!] There was no --query specified." << endl;
			}
		}
		return vm.count("help") ? 0 : 1;
	}

	if (!append && (outGC.empty() && outFP.empty())) {
		cerr << "[Error!] Either --outGC or --outFP should be specified." << endl;
		return 1;
	}

	if (append) {
		if(target.empty()) {
			cerr << "[Error!] --refFP required to append." << endl;
			return 1;
		} else if(!outFP.empty()) {
			cerr << "[Error!] --outFP cannot be specified for appending." << endl;
			return 1;
		}
	}

	if (minANI < 60 || minANI > 100) {
		cerr << "[Error!] minANI should be >= 60 and <= 100" << endl;
		return 1;
	}

	if (cache < 1 || cache > 90) {
		cerr << "[Error!] cache should be >= 1 and <= 90" << endl;
		return 1;
	}

	cache /= 100.;

	if ( minFraction == 0 || (minFraction & ( minFraction-1)) != 0 ) {
		cerr << "[Error!] minFraction (" << minFraction << ") must be a power of 2!" << endl;
		return 1;
	}

	if ( numBits < 2048 || (numBits & ( numBits-1 ) ) != 0 ) {
		cerr << "[Error!] numBits (" << numBits << ") must be a power of 2 and >= 2048!" << endl;
		return 1;
	}
		

	if (verbose)
		gettimeofday(&t1, NULL);

	if (numThreads == 0)
		numThreads = omp_get_max_threads();
	omp_set_num_threads(numThreads);

	Fingerprint::setNumBits(numBits, minFraction);

	std::vector<std::pair<size_t, std::string> > queryFAs;
	std::vector<std::pair<size_t, std::string> > targetFAs; //need to keep to iterate

	size_t nQueries = getNumFPs(query);
	if (nQueries == 0) { //fasta list file
		nQueries = fillFastaList(query, queryFAs);
	} else {
		isQueryFP = true;
	}

	//cout << "isQueryFP: " << isQueryFP << endl;

	//if it is for fp generation, make it and finish
	//todo what if query was fp and want to append??
	if (!outFP.empty()) {
		std::string output_fp_filename = append ? target : outFP;
		std::ofstream of(output_fp_filename.c_str(), append ? std::ios_base::binary | std::ios_base::app : std::ios_base::binary);
		if (!append)
			of.write(reinterpret_cast<char*>(&FP_PREFIX), sizeof(FP_PREFIX));

		FingerprintVector fpSeqs(numThreads);
		verbose_message("Started making fingerprints of %d genomes with %d threads                          \n", nQueries, numThreads);

		ProgressTracker progress(nQueries);

		#pragma omp parallel for schedule (dynamic)
		for (size_t i = 0; i < nQueries; ++i) {
			size_t ii = omp_get_thread_num();

			std::pair<size_t, std::string>& fasta = queryFAs[i];

			//cout << "popcount0: " << i << " : " << ii << " = " << fpSeqs[ii].popcount() << endl;
			fpSeqs[ii].genome_to_bitstring(fasta.second, K, fasta.first);
			//cout << "popcount1: " << ii << " : " << i << " = " << fpSeqs[ii].popcount() << endl;

			#pragma omp critical (FPSEQS_SERIALIZE)
			{
				fpSeqs[ii].serialize(of);
			}

			fpSeqs[ii].clear();

			if (verbose) {
				progress.track();
				if (omp_get_thread_num() == 0 && progress.isStepMarker()) {
					verbose_message("Making fingerprints %s [%.1fGb / %.1fGb]                  \r", progress.getProgress(), getUsedPhysMem(), getTotalPhysMem() / 1024 / 1024);
				}
			}
		}

		of.close();
		verbose_message("Finished making fingerprints of %d genomes.                                  \n", nQueries);

		return 0;
	}

	size_t nTargets = 0;
	if (target.empty()) {
		target = query;
		nTargets = nQueries;
		isTargetFP = isQueryFP;
		self = true;
		targetFAs = queryFAs;
	} else {
		nTargets = getNumFPs(target);
		if (nTargets == 0) { //fasta list file
			nTargets = fillFastaList(target, targetFAs);
		} else {
			isTargetFP = true;
		}
	}

	//cout << "isTargetFP: " << isTargetFP << endl;

	uint64_t fileNumBits = Fingerprint::getNumBits() + sizeof(int) * 8; //bits
	assert(fileNumBits % 8 == 0);
	uint64_t fileNumBytes = fileNumBits / 8;

	std::ifstream fTargetFP;

	if (isTargetFP)
		fTargetFP.open(target.c_str(), std::ios::binary);

	size_t start = 0, end = nTargets;

	if (batchSize > 0) {
		if (batchIndex >= batchSize) {
			cerr << "[Error!] batchIndex cannot be greater or equal to batchSize." << endl;
			exit(1);
		}

		size_t num_refs_batch = ceil(double(nTargets) / batchSize);
		start = batchIndex * num_refs_batch;
		end = std::min((batchIndex + 1) * num_refs_batch, end);
		nTargets = end - start;
	}

	if (isTargetFP)
		fTargetFP.seekg(sizeof(FP_PREFIX) + start * fileNumBytes);

	//* query should be all regardless of idx
	//* target can be subset of those by idx out of start ~ end
	//* need to keep those file handler to control query and fTargetFP

	std::ifstream fQueryFP;

	if (isQueryFP)
		fQueryFP.open(query.c_str(), std::ios::binary);

	size_t SIZE_TARGET_CACHE = std::floor((cache * getTotalPhysMem() * 1024) / fileNumBytes);
	//cout << "SIZE_TARGET_CACHE: " << SIZE_TARGET_CACHE << ", 0.05 * getTotalPhysMem() * 1024: " << 0.05 * getTotalPhysMem() * 1024 << ", fileNumBytes: " << fileNumBytes << endl;
	FingerprintVector querySeqs(numThreads);
	FingerprintVector targetSeqs;

	try {
		targetSeqs.resize(std::min(nTargets, SIZE_TARGET_CACHE));
	} catch(...) {
		cerr << "[Error!] Failed to allocate cache memory. Reduce --cache value." << endl;
		exit(1);
	}

	//every numThreads run search logic below
	size_t max_query_iter = std::ceil((double) nQueries / numThreads);
	size_t leftover_query = nQueries % numThreads;

	ProgressTracker progress(nTargets * nQueries, std::max((size_t)100, std::min((size_t)1000, nTargets * nQueries)));

	size_t max_target_iter = std::ceil((double) nTargets / targetSeqs.size());
	size_t leftover_target = nTargets % targetSeqs.size();
	size_t max_target_size = targetSeqs.size();
	size_t tIdx = 0; //target index

	std::vector<std::ostringstream> oss(numThreads);

	for (size_t t = 0; t < max_target_iter; ++t) {
		if (leftover_target > 0 && t == max_target_iter - 1)
			max_target_size = leftover_target;

		if (isTargetFP) {
			for (size_t j = 0; j < max_target_size; ++j) {
				targetSeqs[j].deserialize(fTargetFP);
				tIdx++;
			}
		} else {
			#pragma omp parallel for schedule (dynamic)
			for (size_t j = 0; j < max_target_size; ++j) {
				#pragma omp atomic
					tIdx++;
				std::pair<size_t, std::string>& fasta = targetFAs[tIdx - 1];
				//cout << fasta.first << " : " << fasta.second.length() << endl;
				targetSeqs[j].clear();
				//cout << "popcount2: " << idx << " : " << targetSeqs.getIndex() << " = " << targetSeqs.popcount() << endl;
				targetSeqs[j].genome_to_bitstring(fasta.second, K, fasta.first);
				//cout << "popcount3: " << t << " : " << targetSeqs[j].getIndex() << " = " << targetSeqs[j].popcount() << endl;
			}
		}

		//cout << "t: " << t << ", max_target_size: " << max_target_size << ", tIdx: " <<  tIdx << endl;

		if (isQueryFP)
			fQueryFP.seekg(sizeof(FP_PREFIX));

		size_t qIdx = 0; //query index
		size_t maxThreads = numThreads;

		//todo if max_query_iter == 1, no need to re-read fp
		for (size_t q = 0; q < max_query_iter; ++q) {
			if (leftover_query > 0 && q == max_query_iter - 1)
				maxThreads = leftover_query;

			if (isQueryFP) {
				for (size_t j = 0; j < maxThreads; ++j) {
					querySeqs[j].deserialize(fQueryFP);
					//cout << "popcountA: " << j << " : " << querySeqs[j].getIndex() << " = " << querySeqs[j].popcount() << endl;
				}
			} else {
				#pragma omp parallel num_threads(maxThreads)
				{
					size_t ii = omp_get_thread_num();

					std::pair<size_t, std::string>& fasta = queryFAs[qIdx + ii];

					querySeqs[ii].clear();

					//cout << "popcount0: " << ii << " : " << querySeqs[ii].getIndex() << " = " << querySeqs[ii].popcount() << endl;
					querySeqs[ii].genome_to_bitstring(fasta.second, K, fasta.first);
					//cout << "popcount1: " << ii << " : " << querySeqs[ii].getIndex() << " = " << querySeqs[ii].popcount() << endl;
				}
			}

			qIdx += maxThreads;

			//cout << "q: " << q << ", maxThreads: " << maxThreads << ", qIdx: " <<  qIdx << endl;

			#pragma omp parallel num_threads(maxThreads)
			{
				size_t ii = omp_get_thread_num();
				for (size_t j = 0; j < max_target_size; ++j) {
					if (!self || (querySeqs[ii].getIndex() < targetSeqs[j].getIndex())) {
						double score = targetSeqs[j].calcScore(querySeqs[ii], metricf);
						if(!noANI)
							score = calc_ani(score);
						if ((!noANI && score >= minANI) || (noANI && score >= minScore)) {
							oss[ii] << querySeqs[ii].getIndex() + 1 << tab_delim << targetSeqs[j].getIndex() + 1 <<
									tab_delim << std::setprecision(6) << score << line_delim;
						}
					}
				}
			}

			if(verbose) {
				progress.track(max_target_size * maxThreads);
				if (omp_get_thread_num() == 0 && progress.isStepMarker()) {
					verbose_message("Calculating GC scores %s [%.1fGb / %.1fGb]\r", progress.getProgress(), getUsedPhysMem(), getTotalPhysMem() / 1024 / 1024);
				}
			}
		}
	}
	verbose_message("Finished calculating GC scores [%d:%d).                                                     \n", start, end);

	if(isQueryFP)
		fQueryFP.close();

	if(isTargetFP)
		fTargetFP.close();

	std::ofstream os(outGC.c_str());
	for(int ii = 0; ii < numThreads; ++ii)
		os << oss[ii].str();
	os.close();

	return 0;
}
