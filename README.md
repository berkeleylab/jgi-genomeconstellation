# Genome Constellation

## Summary
Current supervised phylogeny-based taxonomyclassification methods are time-consuming and fall short on recognizing novel species without close known relatives. To overcome these limitations, we developed a whole-genome sequence based clustering method, “GenomeConstellation,” to rapidly identify large numbers of draft genomes predicted from metagenomics studies.  Genome Constellation calculates similarities between genomes based on their whole genome sequences, and subsequently uses these similarities for classification, clustering and visualization. The clusters of reference genomes formed by Genome Constellation closely resemble known phylogenetic relationships while simultaneously revealing annotation gaps and unexpected phylogenetic relationships.  

## Installation

### download the genome constellation repository
`git clone https://bitbucket.org/berkeleylab/jgi-genomeconstellation.git`


### Optional (to recompile jgi_gc) on a 64-bit Ubuntu Linux system:
* gcc >=4.8 
* boost development libraries with program-options
* libz development libraries 

```
sudo apt-get install build-essential libboost-dev libboost-program-options-dev libz-dev
```

Build jgi_gc executable

```
cd src
make && make install
```

## Usage

Genome Constellation has two major components: a genome spectrum generator and a web app.

#### Spectrum generation

By default, the spectrum generator program is installed in tools/sketching/.
To get help information:

```
tools/sketching/jgi_gc -h

JGI-GC: Genome Constellation (version 0.21.1; Mar 14 2019 17:56:27)
by Don Kang (ddkang@lbl.gov), Rob Egan, Derek Macklin, Jeff Froula, and Zhong Wang

Allowed options:
  -h [ --help ]                        produce help message
  -i [ --query ] arg                   Query file. Either a single column list file or precalculated
                                       fingerprints file. [Mandatory]
  --target arg                         Target file. Either a single column list file or precalculated
                                       fingerprints file.
  --outGC arg                          GC score output file.
  --outFP arg                          Fingerprints output file.
  --minANI arg (=70)                   Minimum ANI threshold to output [60-100]
  --minScore arg (=18.721900000000002) Minimum score
  --noANI                              No ANI transformation. Keep raw GC Score.
  --append                             Append new fingerprints to supplied reference fingerprint file.
  --self                               Indicate query and target are the same. Set to true if target is
                                       missing.
  --cache arg (=50)                    Percentage of system memory allocated for caching [1-90]
  -t [ --numThreads ] arg (=0)         Number of threads to use (0: use all cores)
  --batchIndex arg (=0)                Batch index (0: the first)
  --batchSize arg (=0)                 Batch size (0: no batches)
  --minFraction arg (=1024)            (expert) Minimum hash threshold. 1 kmer out of minFraction bases in
                                       the genome will be added to the fingerprint. It must be power of 2
                                       (1==all)
  --numBits arg (=131072)              (expert) Number of bits in the fingerprint. It must be a power of 2
                                       and >=2048
  -d [ --debug ]                       Debug output
  -v [ --verbose ]                     Verbose output
```

##### Preparing input genome files:  
jgi_gc takes two types of input (-i or --query), either a fasta file or a text file with a list of complete paths to fasta files 


##### Generating spectrums
Example1:

Generate spectrums for a list of fasta sequences:
```
jgi_gc -i sequences_list.txt --outFP fingerprints.fp
```

Example2:

Generate similarity among a list of fasta sequences in infile, keep all scores, do not scale score to ANI%:
```
jgi_gc --query infile --self --outGC outfile.gc --minScore -100 --noANI
```

##### Demo Web App
The web app is based on Python/Flask, a live demo is available at:

http://constellation-classifier.jgi-ga.org/

Or you may get everyting using a docker and run the docker as a web app:
```
docker pull lblzhongwang/jgi-genomeconstellation-app:latest
```
Or you may use a docker with experimental clustering function:
```
docker pull lblzhongwang/jgi-genomeconstellation-app:dev
```

Run the web app and listen to the 8888 port:
```
docker run -p 0.0.0.0:8888:80 lblzhongwang/jgi-genomeconstellation-app:latest
```
The web app should be available at http://localhost:8888

# Contributors
Rob Egan <rsegan@lbl.gov>
Don Kang <donkang34@gmail.com>
Shijie Yao <SYao@lbl.gov>
jeff froula <jlfroula@lbl.gov>
Volkan Sevim <vsevim@lbl.gov>
Zhong Wang <zhongwang@lbl.gov>
