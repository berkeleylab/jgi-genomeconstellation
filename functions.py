# genome constellation functions
# by Zhong Wang @ lbl

import os, subprocess
import pandas as pd
import numpy as np
import json

# config
configs = {
    'reference' : './spectra/7kRef_20bit.fp',
    'annotation': './genome_lists/7kRef_taxa.tsv.gz',
    'nn' : 3,
    'minScore' : 65.0,
    'gc_program' : './jgi_gc'
}

def get_gc_score(list_of_genomes, target, numBits, self=False):
    """
    run jgi_gc on a list of genomes, return spectra
    """
    numBits = 2**numBits
    infile = 'temp_genomes.lst'
    # write list of fasta files
    with open(infile, 'w') as LIST:
        [LIST.write(g + '\n') for g in list_of_genomes]
    # generate query fingerprint
    gcfile = 'temp_genomes.gc' 
    if self:
        jgi_gc = [configs['gc_program'], '--query', infile, '--self', '--outGC', gcfile, '--numBits', str(numBits), '--minANI', str(configs['minScore'])] 
    else:    
        jgi_gc = [configs['gc_program'], '--query', infile, '--target', target, '--outGC', gcfile, '--numBits', str(numBits), '--minANI', str(configs['minScore'])]     
    if subprocess.check_call(jgi_gc) == 0:   
        similarity_matrix = pd.read_csv(gcfile, sep='\t', names=['src', 'des', 'wt'])
        os.remove(gcfile)
        os.remove(infile)
    else:
        print("jgi_gc failed")
        return None    
    return similarity_matrix 


def get_gc_nn_single(sequence, reference, annotation, numBits=20, self=False):
    """
    get genome constellation nearest neighbors
    """
    if self:
        scores = get_gc_score(sequence, target=reference, numBits=numBits, self=True)
    else:    
        scores = get_gc_score([sequence], target=reference, numBits=numBits)
    scores = scores.sort_values('wt', ascending=False)
    scores = scores.rename(columns={'src':'Query', 'wt':'ANI', 'des':'gcID'})
    scores = scores.set_index('gcID')    
    if self:
       # replace query ID with its filename
        queries = {}
        for k,v in zip(range(len(sequence)), sequence):
            queries[k+1] = v.split('/')[-1]
        scores['Query'] = scores['Query'].apply(lambda x: queries[x])
        scores['refID'] = scores.index
        scores['refID'] = scores['refID'].apply(lambda x: queries[x])
    else: 
        nn = configs['nn']
        if scores.shape[0] > nn:
            scores = scores.iloc[0:nn,:]

        # replace query ID with its filename    
        taxa = pd.read_csv(annotation, sep='\t', index_col=0)
        scores['Query'] = sequence.split('/')[-1]
        scores = scores.join(taxa, how='left').reset_index()
    return scores

def make_user_json(scores, outfile=None):
    """
    Make user json files for visualization
    """
    # make nodes
    json_file = {
        "nodes": [],
        "links": []
    }
    for query in list(set(scores["Query"])):
        json_file["nodes"].append({
            "id": query,
            "reference": 0,
            "Annotation": "user genomes"
        })
    for row in scores.iterrows():
        json_file["links"].append({
            "source": row[1][1],
            "target": row[1][3],
            "value": row[1][2]
        })
    if outfile:
        with open(outfile, 'w') as fp:
            json.dump(json_file, fp)    
    return json_file


        