/*
 *  dependencies:
 *  scripts/vivagraph.js
 *  d3.js
 *  jquery.min.js
 *  circleNodeShader.js
 */

(function( window ) {

    //ellipsis universe params
    let ellipseRy = 6000; //4000
    let ellipseRx = 7000; //6000
    let ellipseRy2 = ellipseRy * ellipseRy
    let ellipseRx2 = ellipseRx * ellipseRx
    let currentZoomFactor = 1;


    let DEF_COLOR_MAP = {
        'Archaea' : '#00cc00',      //green
        'Bacteria' : '#9999ff',
        'Eukaryota' : '#ffff00',    //yellow
        'Viruses' : '#ff0000'
    }

    let ALPHA_MASK_DIM = 0x00000044
    let ALPHA_MASK_FAINT = 0x00000022

    function selectText(tipId, txt, e) {
        $('#'+tipId).html(txt)

        let doc = document,
            text = doc.getElementById(tipId),
            range,
            selection;
        if (doc.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        } else if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }

    //-- the main JGI class
    let main = function(){
        this.inrun = true
        this.addNodesToGraphState = -1
        this.addRefLinkCompleted = false

        this.bgcolor = ""
        this.tipId = null

        this.viewid = null
        this.dataNodes = null
        this.dataLinks = null

        this.excludedNodes = []
        this.excludedLinks = []

        this.nodesInGraph = {}   // uas object for lookup performance
        this.linksInGraph = {}

        this.nodesOn = {}
        this.linksOn = {}
        //this.nodesOff = [];
        this.displayLinks = false;
        this.linkAlpha = "ff"
        this.linkColor = "0xffffff"     // full white
        this.dimAlpha = "44";             // display unselected nodes as dimmed


        this.nodeMeta = {}

        this.shiftKeyDown = false;     // shift key status; true = down

        this.springCoeffFactor = 0.002
        this.VIEW_PARAM = {
            "nodeSize" : 25,
            "aniCutOff" : 10,           // add link only when link.value >= this value
            "minLink" : 5,
            'taxLevel' : 'phylum',
            "springLength" : 30,        // view layout setting
            "springCoeff" : 0.5,        // contraction in UI
            "antigravity" : 5,          // dispersion in UI
            "theta" : 0.8,              // same
            "dragCoeff" : 0.02,         // same
            "timeStep" : 20,            // same
        }

        // color generator
        let category200 = '9086fa55ff07fa763709f2fefe59d18aa579fecd0fd3bed571a5bb88eaadbd8588f2ba9096bc02c2952ad992fea8c0feafd0d0ff69839d91b143c1a8bfcd81fd9dcc51c36c5aa5fbc7e825c37bb5b6ada228ccfdffa426f89892d292648f999b08ee73a9b2c530fcdae65bffb1a1ec9dc6a4b873fecbb405b9ab714fd41c6eccd395db688799cdc39cb59fa53ce9b6bae6bf61ff9ffb99d3f4cdcaaad775978ab3addf7a67d7b3f3fe9d61a2919719d99cab82c8af927ae0872692bd7186d9c169b387c6c5c5ea83d67bb8e7bccd47fe76b592a6fec4c2e9d0a195d199d5b7a7cd7cff7255b3b620dcccebb1e0df96a9dda45399d90dcd7bdc8fbac8989b8824fda58899afaea6b1a8b49381d289a7b1b176b844998ed0dfa51183a190bab9557494fbb9945742b5dafe6e60fe6ef9b4c9e0abdaa27de7e3a6fe04a4addcceb18f62ddfbb885a0df6fb3fd869fc9afb5bacfbddcd4029e9b69da8ebbf98a696fc9a0bb8ffffcb263dbd25fe166d4b28eb8db848935b5fad1c2856be849e99f80d6c0b46bce56fea5b5feb2a07ba1a66fa2d6ff8a26fbb60487f08fa4ec52c885706aae9bbf93df2ac2d4c4c4d4df814e9dab678dabcda0bf4c70c3dedfacc4bcb9ff46e6bd99d9e278ae6fb8a00e8dc7c03ed97eab9b4a94a8b3bb999fcfadd6a9c48dbadd73cd69faa6d9bb9a9baac4a84ca3beb3a381e3a3a0a0b3a484adb01a86b6feec8cf6ed9bdaf0699889b291f4817f25c8c4caa5fcdfaa779cbfe5b6bcb1b1c0c7e4c13750fcf29c9fc0e2a3f311d7e761fac4a88da485ac53a493f9ad97ca9bab9e44c78882c08ae1b3a220fd55e985aa25d4af';
        category200 = category200.match(/.{6}/g).map(function(x) { return "#" + x; });
        this.color = d3.scale.ordinal()
        .range(category200);

        this.linkValMax = 0
        this.linkValMin = 100000
    }

    //exposed member functions of main
    main.prototype = {
        //constructor: main,

        enforce_universe_shape : function(){
            let reducef = 0.995
            this.graph.forEachNode( (node) => {
                let nodeui = this.graphics.getNodeUI(node.id)
                if (nodeui.position.x*nodeui.position.x / ellipseRx2 + nodeui.position.y*nodeui.position.y/ ellipseRy2 > 1){
                    // node outside ellipse
                    nodeui.position.x *= reducef
                    nodeui.position.y *= reducef
                }
            });
        },

        runSimulation : function(){
            if (this.inrun){
                this.layout.step()
                this.enforce_universe_shape()
                this.rerender()
                setTimeout(()=>{this.runSimulation() }, 50)
            }
        },

        //- remove the node from selected (highlight); and the on-show links to/from the node
        removeFromNodeOnList : function(aid, syncNodes){
            if (aid in this.nodesOn){
                delete this.nodesOn[aid]
                links = this.graph.getLinks(aid)
                if (links) {
                    if (links.length > 0){
                        console.log('remove links ' + aid + ' - ' + links.length + ":" + syncNodes)
                    }

                    for (let i = 0; i < links.length; ++i) {
                        let link = links[i]

                        if (link.id in this.linksOn){
                            delete this.linksOn[link.id]
                            try {
                                this.graphics.releaseLink(link);
                            } catch (err) { console.log(err)}
                        }

                    }

                }
            }
            if (Object.keys(this.nodesOn).length == 0){
                this.dimNodes(false)
            }

            if (syncNodes){
                this.syncOnNodeAndLinkDisplay()
            }
            this.rerender()
        },

        //- add the node into selected (highlight)
        addToNodeOnList : function(nid, evenHiddenNodes){   // if evenHiddenNodes = false, do not add edges with hidden nodes
            console.log('addToNodeOnList : ' + nid)

            if (!this.isHidden(nid) || evenHiddenNodes) {
                this.nodesOn[nid] = true;
                this.syncOnNode(nid, evenHiddenNodes)
                this.syncOnNodeAndLinkDisplay()
                this.rerender()
            } else {
                console.log('addToNodeOnList - ignore hidden node : ' + nid)
            }

        },

        //- helper to reset the link list for display
        syncOnNode : function(nid, evenHiddenNodes){
            function syncOneNode(self, nid) {
                let node = self.graph.getNode(nid)
                let links = node.links

                if (links && links.length > 0){
                    for (let i = 0; i < links.length; ++i) {
                        let link = links[i];
                        let toUI = self.graphics.getNodeUI(link.toId)
                        let fromUI = self.graphics.getNodeUI(link.fromId)

                        if ( evenHiddenNodes || ( !(self.isHidden(link.toId)) && !(self.isHidden(link.fromId)) ) ){
                            let spring = self.layout.getSpring(link.fromId, link.toId)
                            if (spring.weight != 0) {   // Not a link that eliminated from use by ANI cutoff
                                let linkPosition = self.layout.getLinkPosition(link.id);

                                if ( !(link.id in self.linksOn)){
                                    self.linksOn[link.id] = true
                                    self.graphics.addLink(link, linkPosition)
                                }
                            }
                        }
                    }
                } else {
                    console.log(' -- singleton : ' + nid)
                }
            }

            if (nid) {
                syncOneNode(this, nid)
            } else {
                for (let nid in this.nodesOn) {
                    this.linksOn = {}
                    syncOneNode(this, nid)
                }
            }
        },

        syncOnNodeAndLinkDisplay : function(){
            if (Object.keys(this.nodesOn).length > 0){
                this.dimNodes(true) //initialize all nodes to be dimmed
                for (let nid in this.nodesOn){
                    let node = this.graph.getNode(nid)
                        let nodeui = this.graphics.getNodeUI(nid)
                        delete(node.hide)
                        nodeui.color = this.__toSolidColor(nodeui.color)  // make solid color

                    let links = node.links
                    for (let i = 0; i < links.length; ++i) {
                        let link = links[i];
                        if (link.id in this.linksOn){
                             let toUI = this.graphics.getNodeUI(link.toId)
                            let fromUI = this.graphics.getNodeUI(link.fromId)

                            if ( !(this.isHidden(link.toId)) && !(this.isHidden(link.fromId)) ){
                                let spring = this.layout.getSpring(link.fromId, link.toId)
                                if (spring.weight != 0) {   // Not a link that eliminated from use by ANI cutoff
                                    let linkPosition = this.layout.getLinkPosition(link.id);
                                    let toNode = this.graph.getNode(link.toId)
                                    let fromNode = this.graph.getNode(link.fromId)
                                    delete(toNode.hide)     // turn a possible hidden atom to full color
                                    delete(fromNode.hide)

                                    toUI.color = this.__toSolidColor(toUI.color)
                                    fromUI.color = this.__toSolidColor(fromUI.color)
                                }
                            }
                        }
                    }
                }
            } else {
                this.dimNodes(false)    // undim all nodes
            }
        },

        taxname2Color : function(taxname){
            let clr = '#ffffff'

            if (taxname in DEF_COLOR_MAP){
                clr = DEF_COLOR_MAP[taxname]
            } else if (taxname){
                clr = this.color(taxname)
            } else {
                clr = '#ff0000'
            }
            return clr
        },

        updateNodeMeta : function(taxname, skipTax){
            let clr = this.taxname2Color(taxname)
            if (!skipTax) {
                if ( !(taxname in this.nodeMeta) ){
                    this.nodeMeta[taxname] = {}
                }

                if ( !('color' in this.nodeMeta[taxname]) ){
                    this.nodeMeta[taxname]['color'] = clr
                }

                if ( !('count' in this.nodeMeta[taxname]) ){
                    this.nodeMeta[taxname]['count'] = 1
                } else {
                    this.nodeMeta[taxname]['count'] += 1
                }
            }

            clr = '0x'+clr.substr(1) + 'ff'  // hex value in form of 0xRRGGBBAA
            return clr
        },

        __hex2num : function(hexstr, alpha){
            // hexstr must be in "0xRRGGBBAA" and alpha must be a tring[2] of "AA", like "ff" for solid, and "99"
            let hex = hexstr.slice(0,8) + alpha
            return parseInt(hex, 16)
        },

        __toSolidColor : function(acolor){
            return (acolor | 0x000000ff)
        },

        __toDimColor : function(acolor){
            //return ( ( acolor & 0xffffff00 ) | ALPHA_MASK_DIM )
            return ( ( acolor & 0xffffff00 ) | parseInt('0x000000' + this.dimAlpha, 16) )
        },

        __toFaintColor : function(acolor){
            return ( ( acolor & 0xffffff00 ) | ALPHA_MASK_FAINT )
        },

        __toHideColor : function(acolor){
            return ( acolor & 0xffffff00 )
        },

        isHidden : function(nid){
            let nui = this.graphics.getNodeUI(nid)
            return ( ( nui.color & 0x000000ff ) === 0 )
        },

        isFaint : function(nid){
            let nui = this.graphics.getNodeUI(nid)
            return ( ( nui.color & 0x000000ff ) === ALPHA_MASK_FAINT )
        },

        isFullColor : function(nid){
            let nui = this.graphics.getNodeUI(nid)
            return ( ( nui.color & 0x000000ff ) === 0x000000ff )
        },

        dimNodes : function(dim){   // toggle nodes to be dimmed (dim=true) or full color
            let alpha_mask = parseInt('0x000000' + this.dimAlpha, 16)   //ALPHA_MASK_DIM
            if (!dim){
                alpha_mask = 0x000000ff
            }

            // dim all visible nodes
            this.graph.forEachNode((node)=>{
                let ui = this.graphics.getNodeUI(node.id)
                if ( !this.isHidden(node.id) ){ // don't alter hidden nodes
                    ui.color = (ui.color & 0xffffff00) | alpha_mask
                }
            })
        },



        init_display : function(viewid, tipId) {
            this.graph = Viva.Graph.graph()
            this.graph.Name = "Genome Constellation";
            this.viewid = viewid
            this.tipId = tipId

            //https://github.com/anvaka/ngraph.physics.simulator/blob/master/index.js
            let layout_setting = {
                //Ideal length for links (springs in physical model).
                springLength: this.VIEW_PARAM['springLength'],
                //Hook's law coefficient. 1 - solid spring.
                springCoeff: this.VIEW_PARAM['springCoeff'] * this.springCoeffFactor, // 1 maps to 0.002
                /**
                * Coulomb's law coefficient. It's used to repel nodes thus should be negative
                * if you make it positive nodes start attract each other :).
                */
                //gravity: -1.2, //default: -1.2
                antigravity: -this.VIEW_PARAM['antigravity'],
                /**
                * Theta coefficient from Barnes Hut simulation. Ranged between (0, 1).
                * The closer it's to 1 the more nodes algorithm will have to go through.
                * Setting it to one makes Barnes Hut simulation no different from
                * brute-force forces calculation (each node is considered).
                */
                theta: this.VIEW_PARAM['theta'],
                /**
                * Drag force coefficient. Used to slow down system, thus should be less than 1.
                * The closer it is to 0 the less tight system will be.
                */
                dragCoeff: this.VIEW_PARAM['dragCoeff'], //default: 0.02
                //Default time step (dt) for forces integration
                timeStep : this.VIEW_PARAM['timeStep'],
                springTransform: (link, spring) => {
                    spring.length = this.VIEW_PARAM['springLength'] * (1 - link.data.connectionStrength);   // high ANI => shorter Spring Length
                    spring.weight = link.data.connectionStrength    // high ANI => stronger spring
                }
            };
            this.layout = Viva.Graph.Layout.forceDirected(this.graph, layout_setting);
            this.graphics = Viva.Graph.View.webglGraphics();

            //- display circle shape nodes
            let circleNode = buildCircleNodeShader();
            this.graphics.setNodeProgram(circleNode);

            this.graphics
                .node( (node) => {
                    let hcolor = '0xffffffff'   // solid white color

                    if(node.data.reference == 1){
                        hcolor = this.updateNodeMeta(node.data.taxonomy.phylum)
                    }

                    node.hexcolor = hcolor  // remember the hex color string in node
                    return Viva.Graph.View.webglSquare(15, parseInt(hcolor, 16));

                })
                .link((link)=>{
                    return Viva.Graph.View.webglLine(parseInt(this.linkColor + this.linkAlpha, 16));
                });

            this.renderer_settings = {
                layout     : this.layout,
                graphics   : this.graphics,
                //container  : containerObj,    //document.body,
                interactive: true,
                //interactive: 'node drag',   // disable scroll-zoom : it didn't maintain view center!
                renderLinks: false,
                prerender  : 0
            };

            if (this.viewid === undefined) {
                this.renderer_settings.container = document.body
                this.bgcolor = $(document.body)
            } else {
                this.renderer_settings.container = document.getElementById(viewid)
                this.bgcolor = $('#'+this.viewid).css("background-color")
            }

            this.renderer = Viva.Graph.View.renderer(this.graph, this.renderer_settings);

            this.renderer.run();

            // pre-zoom
            (()=>{
                //let tscale = 0.04    // a expiremental value. 0.1 is good for 4000x6000; 0.04 is good for 6000x7000
                let tscale = 0.09
                while (tscale < currentZoomFactor) {
                    currentZoomFactor = this.renderer.zoomOut();
                }
            })()

            this.renderer.pause()

            // hajack renderer's run and pause function so we can check after each run step
            this.renderer.run = this.runSimulation
            this.renderer.pause = () => { this.inrun = false }
            this.renderer.resume =  () => { this.inrun = true; this.runSimulation() }

            let events = Viva.Graph.webglInputEvents(this.graphics, this.graph)

            events.mouseEnter((node, e)=>{
                if (this.isHidden(node.id)){    // do nothing on  hidden nodes
                    return
                }

                $(document.body).css('cursor', 'pointer')
                let label = 'Unknown'
                if ('taxonomy' in node.data && this.VIEW_PARAM['taxLevel'] in node.data.taxonomy){
                    label = node.data.taxonomy[this.VIEW_PARAM['taxLevel']]
                }

                let title = ''
                let taxonTxt = ''
                if(node.data.reference == 1){
                    taxonTxt = [ node.data.taxonomy.superkingdom,
                                node.data.taxonomy.phylum,
                                node.data.taxonomy.class,
                                node.data.taxonomy.order,
                                node.data.taxonomy.family,
                                node.data.taxonomy.genus,
                                node.data.taxonomy.species
                               ].join('; ')
                    title = `[ ${label} ] - ${node.data.id}`;
                } else {
                    taxonTxt = node.data.annotation
                    if (node.data.group !== undefined){
                        title = `${node.data.group} - ${node.data.id}`
                    } else {
                        title = node.data.id
                    }
                }
                if (taxonTxt){
                    title = `${title} \n ${taxonTxt}`
                }
                document.body.setAttribute("title", title);

                selectText(tipId, title.replace('\n', '<br />'), e)


            }).mouseLeave(function (node, e) {
                $(document.body).css('cursor', 'default')
                selectText(tipId, 'Cursor not on node', e)
                document.body.removeAttribute("title");
            }).dblClick(function (node) {
                if(node.data.reference == 1)
                    window.open(`https://www.ncbi.nlm.nih.gov/genome/?term=${node.data.id}`, '_blank')
                    return false
            }).click( (node) => {
                console.log('on node id : ' + node.data.id)

                if (!this.addRefLinkCompleted || !this.displayLinks){  // do not display link on click if adding ref link not complete
                    return
                }

                if (this.isHidden(node.id)){
                    return
                }

                if (node.id in this.nodesOn) { //removing
                    this.removeFromNodeOnList(node.id, true)
                } else { //adding
                    if (!this.shiftKeyDown){    // remove current displayed links
                        this.clearOnNodes()
                    }

                    this.addToNodeOnList(node.id)
                    this.delayedRerender(20)
                }

                if (Object.keys(this.nodesOn).length == 0){
                    this.dimNodes(false)
                }
                this.renderer.rerender();
            });
        },

        // a unique link ID connecting the given 2 nodes (undirectional)
        unifiedLinkID : function(toID, fromID) { return [toID, fromID].sort().join('-')},

        //- helper for addLinks (build data tree)
        addLinksToGraph : function(fname, llist, isRef, blockSize, lastIndex, addedCnt, callback, delay){
            let i = lastIndex;
            let bsize = blockSize

            // local helpers
            function addLink(self, link, strength, linkId, cnt){
                self.linkValMax = Math.max(self.linkValMax, link.value)
                self.linkValMin = Math.min(self.linkValMin, link.value)
                self.graph.addLink(link.source, link.target, { connectionStrength: strength, "value" : link.value });
                self.linksInGraph[linkId] = true
                cnt++
                return cnt
            }

            for (; i < llist.length && bsize>0; ++i, bsize--){
                let link = llist[i];
                let strength = link.value / 100.0
                if (strength >= 1){ strength = 1.0 } else if (strength <= 0) { strength = 0.01 }
                let uLinkId = this.unifiedLinkID(link.source, link.target)

                if ( uLinkId in this.linksInGraph ) {  // be sure a unique link is only added once
                    continue
                }

                if (!isRef){
                    // to and from nodes must be already in graph
                    if (link.value >= this.VIEW_PARAM['aniCutOff'] && this.nodesInGraph.hasOwnProperty(link.source) && this.nodesInGraph.hasOwnProperty(link.target)){
                        addedCnt = addLink(this, link, strength, uLinkId, addedCnt)
                    }
                } else {
                    if (link.value >= this.VIEW_PARAM['aniCutOff'] && this.nodesInGraph.hasOwnProperty(link.source) && this.nodesInGraph.hasOwnProperty(link.target)){
                        addedCnt = addLink(this, link, strength, uLinkId, addedCnt)
                    } else {
                        this.excludedLinks.push(link)
                    }
                }
            }

            if (callback != undefined){
                let pct = (lastIndex / llist.length) * 100
                callback({"filename": fname, "percent": pct.toFixed(2), "links" : lastIndex})
            }
            lastIndex = i

            if (lastIndex < llist.length){
                setTimeout( () => {this.addLinksToGraph(fname, llist, isRef, blockSize, lastIndex, addedCnt, callback, delay) }, delay) // .05 sec
            } else if (callback != undefined) {
                console.log('lastIndex='+lastIndex+'; length='+llist.length)
                callback({"filename": fname, "total": addedCnt, "links" : ""})
            }

            if (!isRef){
                //console.log('ADDED user data link :' + addedCnt)
            }
        },

        updateNodeSize : function(aval){
            this.graph.forEachNode( (node) => {
                let nui = this.graphics.getNodeUI(node.id)
                if ( !('oricolor' in nui) ){
                    nui.size = aval
                }
            })
            this.rerender()
        },

        //- build data tree
        addNodes : function(nlist, atype, callback, hasXY){
            let st = Date.now()
            let nodecnt = 0
            let refcnt = 0

            //node color
            let groups = d3.nest()
            .key(function(d) { if ('taxonomy' in d) {return d.taxonomy.phylum} else {return ''} })
            .entries(nlist)
            .sort(function(a, b) { return b.values.length - a.values.length; });

            this.color.domain(groups.map(function(d) { return d.key; }));

            for (let i = 0; i < nlist.length; ++i){
                let node = nlist[i];
                if ( atype != 'ref' ){
                    //node.reference == 0
                    this.nodesInGraph[node.id] = 1
                    let anode = this.graph.addNode(node.id, node);
                    //if (anode.data.group === undefined){
                        anode.user = atype
                    //} else {
                    //    anode.user = `user-${anode.data.group}`;
                    //}

                    nodecnt++
                } else {
                    if (!hasXY || (node.x != undefined && node.y != undefined) ){
                        nodecnt++
                        if (node.reference == 1){
                            refcnt++
                        }
                        let anode = this.graph.addNode(node.id, node);

                        this.nodesInGraph[node.id] = 1
                    } else {
                        this.excludedNodes.push(node)
                    }
                }
            }

            if ( atype==='ref' ){
                let dt = Date.now() - st
                console.log('Add nodes time (ms): ' + dt)   // 83msec
                this.addNodesToGraphState = 1
            }
            this.updateNodeSize(this.VIEW_PARAM['nodeSize'])
            if (callback != undefined){
                callback({'nodes': nodecnt, 'refs' : refcnt})
            }

            return true
        },

        //- build data tree
        addLinks : function(fname, llist, isRef, callback, delay){
            if (isRef && this.laddNodesToGraphState == -2){
                updateProgress('Failed to add nodes to graph!')
                return
            } else if (this.addNodesToGraphState==1){
                let renderLinks = this.renderer_settings.renderLinks

                this.addLinksToGraph(fname, llist, isRef, 5000,  0, 0, callback, delay)
                //this.renderer_settings.renderLinks = renderLinks

            } else {
                //console.log("Node not ready. Skip adding links.")
            }
        },

        setAddLinkStatus : function(state){ this.addRefLinkCompleted = state},

        applyNodePositions : function(){
            this.graph.forEachNode( (node) => {
                nodeui = this.graphics.getNodeUI(node.id)
                if (node.data.x && node.data.y){
                    nodeui.position.x = node.data.x
                    nodeui.position.y = node.data.y
                }
            });
        },

        zoomFit : function(targetScale, currentScale, running) {
            this.rerender()
            if (targetScale < currentScale) {
                currentScale = this.renderer.zoomOut();

                setTimeout( ()=>{
                    this.zoomFit(targetScale, currentScale, running);
                }, 15);
            } else {
                if (running){
                    this.resume()
                }
            }
        },

        fitScreen : function(){
            this.renderer.reset()
            //renderer.moveTo(0,0)
            let targetscale = this.desiredScale()
            let running = this.inrun
            if (running){
                this.renderer.pause()
            }
            this.zoomFit(targetscale, 1, running);
        },

        desiredScale : function(){
            let graphRect = this.layout.getGraphRect();
            let graphSize = Math.min(graphRect.x2 - graphRect.x1, graphRect.y2 - graphRect.y1);
            let jqobj = $("canvas")
            let screenSize = Math.min(jqobj.width(), jqobj.height());
            let scalefactor = 0.9 // practival observation tells we need to overshut a little
            return (screenSize / graphSize) * scalefactor;
        },

        nodePositions : function(){
                let pos = {}
                this.graph.forEachNode(function(node) {
                    let nodeui = this.graphics.getNodeUI(node.id)
                    pos[node.id] = [nodeui.position.x, nodeui.position.y]
                });
                return JSON.stringify(pos)

            },

        currentNodeData : function() {
                let nodelist = []
                this.graph.forEachNode( (node) => {
                    let nodeui = this.graphics.getNodeUI(node.id)
                    let dnode = node.data
                    dnode.x = nodeui.position.x
                    dnode.y = nodeui.position.y
                    nodelist.push(dnode)
                });

                for (let n=0; n<excludedNodes.length; n++){
                    nodelist.push(excludedNodes[n])
                }
                return JSON.stringify({'nodes': nodelist})
            },

        graphLimits : function(){
                let xmax = 0
                let xmin = 0
                let ymax = 0
                let ymin = 0
                this.graph.forEachNode(function(node) {
                    let nodeui = this.graphics.getNodeUI(node.id)
                    xmax = Math.max(xmax, nodeui.position.x)
                    xmin = Math.min(xmin, nodeui.position.x)
                    ymax = Math.max(ymax, nodeui.position.y)
                    ymin = Math.min(ymin, nodeui.position.y)
                });
                return {'xmax': xmax, 'xmin': xmin, 'ymax': ymax, 'ymin': ymin}
            },

        __nodeTaxName : function(taxlevel, node){   // for a given taxlevel and a node, return the taxname in the node. ONLY FOR ref data!
            let taxval = null
            if (node.data.reference == 1){
                switch(taxlevel){
                    case "superkingdom" : taxval = node.data.taxonomy.superkingdom; break;
                    case "phylum"       : taxval = node.data.taxonomy.phylum; break;
                    case "class"        : taxval = node.data.taxonomy.class; break;
                    case "order"        : taxval = node.data.taxonomy.order; break;
                    case "family"       : taxval = node.data.taxonomy.family; break;
                    case "genus"        : taxval = node.data.taxonomy.genus; break;
                    case "species"      : taxval = node.data.taxonomy.species; break;
                }
            }

            return taxval
        },

        updateNodeColorByTax : function(taxlevel, reset){
            console.log("updateNodeColorByTax（） : taxlevel=" + taxlevel + "; reset=" + reset)
            this.nodeMeta = {}
            this.graph.forEachNode( (node) => {
                let nodeui = this.graphics.getNodeUI(node.id)

                if ( node.data.reference == 1) {    //only change reference nodes !!

                    if (!reset && this.isHidden(node.id)) {   // do nothing if node already hidden and not reset
                        return
                    }

                    let taxval = this.__nodeTaxName(taxlevel, node)

                    if (taxval){
                        //== this algorithm resets all nodes and only
                        let hcolor = this.updateNodeMeta(taxval, node.hide)
                        node.hexcolor = hcolor  // remember the hex color string in node
                        nodeui.color = parseInt(hcolor, 16)
                        if (node.hide){
                            nodeui.color = this.__toHideColor(nodeui.color)
                        }
                    }
                }
            });
        },

        setNodeColor : function(atype, aname, acolor){ //type can be taxlevel or user, aname can be taxname or user label, color in #rrggbb

            function setNodeColor(node, nui, acol){
                let ncolor = acol + "ff"
                node.hexcolor = ncolor
                nui.color = parseInt(ncolor, 16)
            }

            this.graph.forEachNode( (node) => {
                let nodeui = this.graphics.getNodeUI(node.id)
                if (aname.toLowerCase().indexOf('user-') === 0){
                    if (node.user === aname) {
                        setNodeColor(node, nodeui, acolor)
                    }
                } else {    // taxlevel
                    let taxval = this.__nodeTaxName(atype, node)
                    if (taxval === aname ){
                        setNodeColor(node, nodeui, acolor)
                    }
                }
            })
            this.rerender()
        },

        setConfigValue : function(akey, aval, needSimulate){
            let simulate = false
            if (this.VIEW_PARAM[akey] != aval){
                this.VIEW_PARAM[akey] = aval
                switch (akey){
                    case 'springLength' :   this.layout.simulator.springLength(aval)     // value of 50 - 1 has no effect on simulation !
                                            simulate = true
                                            break;
                    case 'springCoeff'  :   this.layout.simulator.springCoeff(aval * this.springCoeffFactor)
                                            simulate = true
                                            break;
                    case 'antigravity'      :   this.layout.simulator.gravity(-aval)
                                            simulate = true
                                            break;
                    case 'theta'        :   this.layout.simulator.theta(aval)
                                            simulate = true
                                            break;
                    case 'dragCoeff'    :   this.layout.simulator.dragCoeff(aval)
                                            simulate = true
                                            break;
                    case 'timeStep'     :   this.layout.simulator.timeStep(aval)
                                            simulate = true
                                            break;
                    case 'taxLevel'     :   this.updateNodeColorByTax(aval)
                                            break;
                    case 'nodeSize'     :   this.updateNodeSize(aval)
                                            break;
                }
            }
            return (needSimulate || simulate)
        },

        updateUniverseSize : function(univx, univy){
            if (univx != ellipseRx || univy != ellipseRy){
                ellipseRx = univx; ellipseRy = univy;
                ellipseRx2 = univx*univx; ellipseRy2=univy*univy;
                return true
            } else {
                return false
            }
        },

        inSimulation : function() { return this.inrun; },

        toggleSimulation : function() { if (this.inrun) {this.renderer.pause() } else {this.renderer.resume() } },

        zoomIn : function() { return this.renderer.zoomIn() },

        zoomOut : function() { return this.renderer.zoomOut() },

        pause : function() { this.renderer.pause() },

        resume : function() { this.renderer.resume() },

        // remove links is async - use delayedRerender to redraw the screen after links are removed
        delayedRerender : function(delay) { setTimeout(()=>{this.rerender()}, delay) },

        rerender : function() {
            //console.log('rerender is called');
            this.renderer.rerender()
        },

        clearOnNodes : function(){
            let nids = []
            if (Object.keys(this.nodesOn).length > 0){
                for (let nid in this.nodesOn) {
                    nids.push(nid)
                    this.removeFromNodeOnList(nid, false)    // do not sync nodes
                }
                this.dimNodes(false)
            }
            return nids
        },

        //removeNodeFromOnList : function(nid){
        //    if (nid in this.nodesOn){
        //        this.removeFromNodeOnList(nid, true)
        //    }
        //    if (Object.keys(this.nodesOn).length == 0){
        //        this.dimNodes(false)
        //    }
        //    this.rerender();
        //},

        showLinks : function(nid){
            if (Object.keys(this.nodesOn).length == 0){
                this.dimNodes(true)
            }
            this.addToNodeOnList(nid, true)
        },

        setShiftKey : function(state) { this.shiftKeyDown = state; },

        toggleShowLink : function() {
            if (this.addRefLinkCompleted) {this.displayLinks = this.renderer.toggleLink()} else {alert("Please wait for adding links to complete!")};

            if (!this.displayLinks && Object.keys(this.nodesOn).length > 0){  // from ON to OFF
                this.clearOnNodes()

                // make all visible nodes display normal
                this.graph.forEachNode( (node) => {
                    let nodeui = this.graphics.getNodeUI(node.id)

                    if ( !this.isHidden(node.id) ) {
                        nodeui.color = this.__toSolidColor(nodeui.color)
                    }
                })
            }
            this.delayedRerender(20)
            return this.displayLinks
        },

        //- tax group turned on/off
        showSubNodes : function(aName, hide){
            //aName can be a taxname or "User Data"
            console.log('showSubNodes（） tax : ' + aName + '; ' + hide)
            //this.clearOnNodes()    // remove currently shown links
            //this.dimNodes(false)
            let running = this.inrun
            if (running){
                this.pause()
            }

            this.graph.forEachNode( (node) => {
                let nodeui = this.graphics.getNodeUI(node.id)
                if ( ( aName.toLowerCase().indexOf('user-') === 0 && node.user === aName )                          ||
                     ( ('taxonomy' in node.data) && (node.data['taxonomy'][this.VIEW_PARAM['taxLevel']] == aName) ) ||
                     ( aName.toLowerCase() == 'all' ) ) {
                    if ( hide ) {
                        //set color transparent
                        nodeui.color = this.__toHideColor(nodeui.color)
                    } else if ( !(node.hide) ) {
                        nodeui.color = this.__toSolidColor(nodeui.color)  // solid display
                    }
                }
            })

            if (aName.toLowerCase() === 'all' && !hide){
                this.updateNodeColorByTax(this.VIEW_PARAM['taxLevel'], true)
            }

            //- reset the NodeOn list and links
            let onlist = this.clearOnNodes()
            for (let idx in onlist) {
                this.addToNodeOnList(onlist[idx])    // use (nid, true) if to show Nodes even it is has been set to hidden
            }
            this.syncOnNodeAndLinkDisplay()

            if (running){
                this.resume()
            }
            this.delayedRerender(50)

        },

        removeUserData : function(node){
            this.graph.forEachNode((node)=>{
                if(!node.data.reference){
                    setTimeout(()=>{
                        this.graph.removeNode(node.id)
                        this.rerender()
                    }, 100)
                }
            })
        },

        __update_ani : function(llist, ani, blockSize, lastIndex, callback, removedCnt, delay){
            let i = lastIndex;
            let bsize = blockSize

            for (; i < llist.length && bsize>0; ++i, bsize--){
                let link = llist[i];
                let spring = this.layout.getSpring(link.fromId, link.toId)
                if (link.data.value < ani) {
                    spring.weight=0.0
                    removedCnt++
                } else {
                    spring.weight = 1.0
                }
            }

            lastIndex = i

            if (lastIndex < llist.length){
                let pct = ((i/llist.length) * 100.0).toFixed(1)
                if (callback != undefined) {
                    callback({'pct' : pct})
                }
                //console.log(' finished ' + pct + '%')
                setTimeout( () => {this.__update_ani(llist, ani, blockSize, lastIndex, callback, removedCnt, delay) }, delay) // .05 sec
            } else {
                if (callback != undefined) {
                    callback({'total-removed' : removedCnt})
                }
                //console.log("update_ani less count : " + removedCnt)
            }
        },

        update_ani(ani, callback){
            let offedge = 0
            let links = []
            this.graph.forEachLink((link)=>{
                links.push(link)
            })

            //batch process so UI can update progress info to user!
            this.__update_ani(links, ani, 1000, 0, callback, 0, 10)
        },

        export_data : function(){
            let data = Object.keys(this.nodesOn)
            let ret = Object.keys(this.nodesOn)
            if (data.length > 0){
                for (let n=0; n<data.length; n++){
                    let nid = data[n]
                    let links = this.graph.getLinks(nid)
                    if (links){
                        for (let i = 0; i < links.length; ++i) {
                            let link = links[i];

                            if ( !this.isHidden(link.toId) && !this.isHidden(link.fromId) ){
                                if (ret.indexOf(link.toId) == -1){
                                    ret.push(link.toId)
                                } else if(ret.indexOf(link.fromId) == -1){
                                    ret.push(link.fromId)
                                }
                            }
                        }
                    }
                }
            } else {  // no show link selections - collect all visible nodes
                this.graph.forEachNode( (node) => {
                    if ( !this.isHidden(node.id) && !this.isFaint(node.id) ) {
                        ret.push(node.id)
                    }
                })
            }
            return ret
        },

        findOutFillValueList : function(atype, aval){
            let alist = {}
            this.graph.forEachNode(function(node){
                let findNode = null
                if (atype === 'NCBI ID' && node.id.indexOf(aval) == 0 ) {
                    if ( !(node.id in alist) ){
                        alist[node.id] = true
                    }

                } else if ( 'taxonomy' in node.data && node.data.taxonomy[atype] && node.data.taxonomy[atype].indexOf(aval) === 0 ){
                    if ( !(node.data.taxonomy[atype] in alist) ){
                        alist[node.data.taxonomy[atype]] = true
                    }
                }
            })
            console.log('findValueList : ' + atype + '; ' + aval)
            return Object.keys(alist).sort()
        },

        searchNodeIDList : function(atype, val){
            let alist = new Set()
            val = val.toLocaleLowerCase().split(/[\s+, ',', ';']/).filter((e)=>e) // split aval by SPACE, "," and ";", rm empty avlues
            val.forEach((aval)=>{
                    this.graph.forEachNode(function(node){
                    let findNode = null
                    if (atype === 'NCBI ID' && node.id.toLocaleLowerCase().indexOf(aval) == 0 ) {
                        alist.add(node.id)
                    } else if ( 'taxonomy' in node.data && node.data.taxonomy[atype] && node.data.taxonomy[atype].toLocaleLowerCase().indexOf(aval) === 0 ){
                        alist.add(node.id)
                    }
                })
            })

            return Array.from(alist)
        },

        hideSingletons : function( hide ){
            this.graph.forEachNode((node)=>{
                let singleton = true
                for (let n=0; n<node.links.length; n++) {
                    let link = node.links[n]
                    let spring = this.layout.getSpring(link.fromId, link.toId)
                    if (spring.weight != 0){
                        singleton = false
                        break
                    }
                }

                if (singleton){
                    //console.log('hideSingletons ' + node.id)
                    let nodeui = this.graphics.getNodeUI(node.id)
                    if ( hide ) {
                        node.hide = true
                        nodeui.color = this.__toHideColor(nodeui.color)
                    } else {
                        if ('hide' in node){
                            delete(node.hide)
                        }
                        nodeui.color = this.__toSolidColor(nodeui.color)
                    }
                }
            })
            this.rerender()
        },

        //- highlight a given list of nodes (from search)
        selectNodes : function(nlist, unselect){
            this.graph.forEachNode((node)=>{
                let nui = this.graphics.getNodeUI(node.id)
                if (!unselect){
                    if (nlist.indexOf(node.id) == -1 && !this.isHidden(node.id) ){  // do not change hidden nodes
                        node.unselected = true
                        nui.color = this.__toDimColor(nui.color)
                    } else {
                        delete(node.unselected)
                        nui.color = this.__toSolidColor(nui.color)
                    }
                } else if ( !this.isHidden(node.id) ) {
                    delete(node.unselected)
                    nui.color = this.__toSolidColor(nui.color)
                }
            })

            this.syncOnNodeAndLinkDisplay()
            this.rerender()
        },

        hasLinks : function(nid1, nid2){
            if (!nid1 || !nid2) {
                console.log("Need 2 node IDs as args")
                return
            }
            let conn = []
            let link = this.graph.getLink(nid1, nid2)
            if (link) {
                conn.push(link.id)
            }
            link = this.graph.getLink(nid2, nid1)
            if (link) {
                conn.push(link.id)
            }
            return conn
        },

        redrawLinks : function(){
            for (let lid in this.linksOn) {
                let ui = this.graphics.getLinkUI(lid)
                ui.color = parseInt(this.linkColor + this.linkAlpha, 16)
            }
            this.rerender()
        },

        setLinkColor : function(color){ // color from HTML is "0xRRGGBB"
            this.linkColor = color
            this.redrawLinks()
        },

        setDimAlpha : function(alpha){
            this.dimAlpha = alpha
            if (Object.keys(this.nodesOn).length > 0) {
                // update node
                this.graph.forEachNode( (node) => {
                    let nodeui = this.graphics.getNodeUI(node.id)
                    nodeui.color = this.__toDimColor(nodeui.color)
                })
                this.syncOnNodeAndLinkDisplay()
                this.rerender()
            }
        },

        setLinkAlpha : function(alpha){
            this.linkAlpha = alpha
            this.redrawLinks()
        },
    }



    // Create a main object and put it i global scope
    return (window.JGI = new main());

})(window)