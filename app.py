## this is the main web app
# by ZHONGWANG @ lbl . gov

from flask import Flask, flash, request, render_template, redirect
from werkzeug.utils import secure_filename
import zipfile, gzip
from functions import *
import json

application = Flask(__name__, 
                    static_url_path='',
                    static_folder='www',
                   )

UPLOAD_FOLDER = 'genomes'


application.secret_key = "secret key"
application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
application.config['MAX_CONTENT_LENGTH'] = 200 * 1024 * 1024


ALLOWED_EXTENSIONS = set(['fa', 'fna', 'fasta', 'zip', 'gz'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def handle_files(filename):
    """
    if uploaded file is a single fasta file, return it
    if uploaded file is zipped or gzipped, return the list of files
    """
    ftype = filename.rsplit('.', 1)[1].lower()
    if (ftype == 'fna') or (ftype == 'fa'):
        return [filename]
    if ftype == 'zip':
        files = []
        with zipfile.ZipFile(filename, mode='r') as zf:
            for sequence in zf.namelist():
                fullpath = os.path.join(application.config['UPLOAD_FOLDER'], sequence)
                zf.extract(sequence, application.config['UPLOAD_FOLDER'])
                files.append(fullpath) 
        os.remove(filename)        
        return files
    elif ftype == 'gz':
        files = []
        with gzip.open(filename, "rt") as f:
            sequence = filename.rsplit('.', 1)[0]
            with open(sequence, "w") as d: 
                d.write(f.read())
                files.append(sequence)
        os.remove(filename)        
        return files
    else:
        flash("file type is not supported")
        return []
    

@application.route('/')
def upload_form():
    return render_template('./upload.html')


@application.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if ('genome1' not in request.files):
            flash('One or more files are missing')
            return redirect(request.url)
        file1 = request.files['genome1']
        if file1.filename == '':
            flash('at least one of the genomes is required')
            return redirect(request.url)
        if allowed_file(file1.filename):
            filename1 = secure_filename(file1.filename)
            file1.save(os.path.join(application.config['UPLOAD_FOLDER'], filename1))
            flash('Files successfully uploaded')

            name1 = request.form['name1']
            if name1 == '':
                name1 = filename1
            configs['name'] = name1
            nn = int(request.form['nn'])
            if nn > 0:
                configs['nn'] = nn
            else:
                nn = configs['nn']
            minScore = float(request.form['minScore'])
            if minScore > 0.0:
                configs['minScore'] = minScore
            reference = request.form['reference']
            if reference == 'img':
                configs['annotation'] = './genome_lists/img100k_gcID_taxa.csv.gz'
                configs['reference'] = './spectra/img100k_genomes_200K_20M_20bit.fp'
            else:
                configs['annotation'] = './genome_lists/7kRef_taxa.tsv.gz'
                configs['reference'] = './spectra/7kRef_20bit.fp'
                
            flash("Now look for the nearest neighbours of " + name1 + ' ...')
            # 
            genome1 = os.path.join(application.config['UPLOAD_FOLDER'], filename1)
            genomes = handle_files(genome1)
            configs['genome'] = genomes
            if len(genomes) < 1:
                flash("Sth. wrong with the input file")
                return redirect(request.url)            
            else:
                results = []
                for g in genomes:
                    result = get_gc_nn_single(g, reference=configs['reference'], annotation=configs['annotation'])
                    results.append(result)
                results = pd.concat(results, axis=0)
            # os.remove(genome1)
            return render_template('show_result.html', tables=[results.to_html(index=False, justify='left', na_rep=' ', classes='blueTable')])

        else:
            flash('Allowed file types are fa, fna, zip, gz')
            return redirect(request.url)

@application.route('/<path:path>')
def static_file(path):
    return application.send_static_file(path)

@application.route('/showmap')
def showmap():
    #configs['nn'] = -1 # for visual, select all neighbors
    results = []
    if 'genome' not in configs: # just show the map
       return render_template('index.html', userdata={}, label='') 
    if len(configs['genome']) == 1:
        results = get_gc_nn_single(configs['genome'][0], reference='./spectra/7kRef_20bit.fp', annotation='./genome_lists/7kRef_taxa.tsv.gz')
    else:
        # user genome to reference
        for g in configs['genome']:
            result = get_gc_nn_single(g, reference='./spectra/7kRef_20bit.fp', annotation='./genome_lists/7kRef_taxa.tsv.gz')
            results.append(result)
        # user genome among themselves
        results.append(get_gc_nn_single(configs['genome'], reference='./spectra/7kRef_20bit.fp', annotation='./genome_lists/7kRef_taxa.tsv.gz', self=True))  
        
        results = pd.concat(results, axis=0, sort=False)
    results.fillna('NA', inplace=True)
    userjson = make_user_json(results)
    return render_template('index.html', userdata=userjson, label=configs['name'])

if __name__ == '__main__':
    application.run(host="0.0.0.0", port=80)
